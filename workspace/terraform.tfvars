# !NOTE! - These are only a subset of the variables in CONFIG-VARS.md provided
# as examples. Customize this file to add any variables from CONFIG-VARS.md whose
# default values you want to change.

# ****************  REQUIRED VARIABLES  ****************
# These required variables' values MUST be provided by the User
prefix                                  = "sas-viya4"
location                                = "ap-southeast-1" # e.g., "us-east-1"
# ****************  REQUIRED VARIABLES  ****************

## Bring your own existing resources
/************************************************** Not needed
vpc_id  = "vpc-0a626ced2371fb911" # only needed if using pre-existing VPC
subnet_ids = {  # only needed if using pre-existing subnets
  "public" : ["subnet-0e55f3bfff933e1f1, "subnet-02bf9a28187b02eba"],
  "private" : ["subnet-00d52b412c9372520", "subnet-0baba6e0d31e1a280"],
  "database" : ["subnet-0b140593c5f0ba39a", "subnet-06445001b4874ed5a"] # only when 'create_postgres=true'
}
nat_id = "<existing-NAT-gateway-id>"
security_group_id = "<existing-security-group-id>" # only needed if using pre-existing Security Group
***********************************************************END*/
# !NOTE! - Without specifying your CIDR block access rules, ingress traffic
#          to your cluster will be blocked by default.

# **************  RECOMMENDED  VARIABLES  ***************
default_public_access_cidrs = ["0.0.0.0/0"]  # e.g., ["123.45.6.89/32"]
ssh_public_key              = "/workspace/.ssh/id_rsa.pub"
# **************  RECOMMENDED  VARIABLES  ***************

# Tags for all tagable items in your cluster.
tags                                    = {"key1" = "eks-sas-viya4"} # e.g., { "key1" = "value1", "key2" = "value2" }

# Postgres config - By having this entry a database server is created. If you do not
#                   need an external database server remove the 'postgres_servers'
#                   block below.
postgres_servers = {
  default = {},
  anotherserver = {
    instance_type                = "db.t4g.xlarge"
    storage_size                 = 50
    storage_encrypted            = false
    backup_retention_days        = 7
    multi_az                     = false
    deletion_protection          = false
    administrator_login          = "cpsadmin"
    administrator_password       = "1tsAB3aut1fulDay"
    server_version               = "13"
    server_port                  = "5432"
    ssl_enforcement_enabled      = true
    #parameters                   = [{ "apply_method": "immediate", "name": "foo" "value": "true" }, { "apply_method": "immediate", "name": "bar" "value": "false" }]
    options                      = []
  },
}

## Cluster config
kubernetes_version                      = "1.24"
default_nodepool_node_count             = 1
default_nodepool_vm_type                = "r5.xlarge"
default_nodepool_custom_data            = ""

## General
efs_performance_mode                    = "maxIO"
storage_type                            = "ha"

## Cluster Node Pools config
node_pools = {
  cas = {
    "vm_type" = "r5.xlarge"
    "cpu_type" = "AL2_x86_64"
    "os_disk_type" = "gp2"
    "os_disk_size" = 200
    "os_disk_iops" = 0
    "min_nodes" = 1
    "max_nodes" = 5
    "node_taints" = ["workload.sas.com/class=cas:NoSchedule"]
    "node_labels" = {
      "workload.sas.com/class" = "cas"
    }
    "custom_data" = ""
    "metadata_http_endpoint"               = "enabled"
    "metadata_http_tokens"                 = "required"
    "metadata_http_put_response_hop_limit" = 1
  },

    compute = {
    "vm_type" = "r5.large"
    "cpu_type" = "AL2_x86_64"
    "os_disk_type" = "gp2"
    "os_disk_size" = 200
    "os_disk_iops" = 0
    "min_nodes" = 1
    "max_nodes" = 5
    "node_taints" = ["workload.sas.com/class=compute:NoSchedule"]
    "node_labels" = {
      "workload.sas.com/class"        = "compute"
      "launcher.sas.com/prepullImage" = "sas-programming-environment"
    }
    "custom_data" = ""
    "metadata_http_endpoint"               = "enabled"
    "metadata_http_tokens"                 = "required"
    "metadata_http_put_response_hop_limit" = 1
  },

  stateless = {
    "vm_type" = "r5.2xlarge"
    "cpu_type" = "AL2_x86_64"
    "os_disk_type" = "gp2"
    "os_disk_size" = 200
    "os_disk_iops" = 0
    "min_nodes" = 1
    "max_nodes" = 5
    "node_taints" = ["workload.sas.com/class=stateless:NoSchedule"]
    "node_labels" = {
      "workload.sas.com/class" = "stateless"
    }
    "custom_data" = ""
    "metadata_http_endpoint"               = "enabled"
    "metadata_http_tokens"                 = "required"
    "metadata_http_put_response_hop_limit" = 1
  },

  stateful = {
    "vm_type" = "r5.xlarge"
    "cpu_type" = "AL2_x86_64"
    "os_disk_type" = "gp2"
    "os_disk_size" = 200
    "os_disk_iops" = 0
    "min_nodes" = 1
    "max_nodes" = 3
    "node_taints" = ["workload.sas.com/class=stateful:NoSchedule"]
    "node_labels" = {
      "workload.sas.com/class" = "stateful"
    }
    "custom_data" = ""
    "metadata_http_endpoint"               = "enabled"
    "metadata_http_tokens"                 = "required"
    "metadata_http_put_response_hop_limit" = 1
  }
}

# Jump Server
create_jump_vm                        = true
jump_vm_admin                         = "jumpuser"
jump_vm_type                          = "t3.medium"

